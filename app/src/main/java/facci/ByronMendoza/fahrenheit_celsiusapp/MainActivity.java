package facci.ByronMendoza.fahrenheit_celsiusapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText temp = (EditText)findViewById(R.id.editText2);
        final RadioButton radio1 = (RadioButton) findViewById(R.id.btnCF);
        final RadioButton radio2 = (RadioButton) findViewById(R.id.btnFC);
        final Button boton = (Button) findViewById(R.id.btnconvertir);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double conversor = new Double(temp.getText().toString());
                if (radio1.isChecked()) {

                        conversor = Convertir.celsiusfarenheit(conversor);
                    Toast.makeText(MainActivity.this, new Double(conversor).toString(), Toast.LENGTH_LONG).show();
                    //temp.setText(new Double(conversor).toString());
                    temp.setText("");

                    }
                else if(radio2.isChecked()) {


                    conversor = Convertir.farecnheitcelcius(conversor);
                    Toast.makeText(MainActivity.this, new Double(conversor).toString(), Toast.LENGTH_LONG).show();
                    //temp.setText(new Double(conversor).toString());
                    temp.setText("");
                }
            }
        });
    }
}
